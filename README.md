# Instruction

This is MQTT communication on Raspberry Pi with C.

---
## Usage
1. Copy MQTT-c-pub-sub-master_success folder to RPi.
2. Run mqtt_publisher to use shell command like this:

        cd MQTT-c-pub-sub-master_success/build
        sudo chmod +x mqttpub
        ./mqttpub
   
3. Run mqtt_subscriber to use shell command like this:

        cd MQTT-c-pub-sub-master_success/build
        sudo chmod +x mqttsub
        ./mqttsub
   
---
## Result
Publish:

![publish](image/mqtt_C_publisher.PNG)

Subscribe:

![subscribe](image/mqtt_C_subscriber.PNG)